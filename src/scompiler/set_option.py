import argparse

def set_options():
	# Arguments definitions
	arg_action = [
		["--version", "Print the version"],
		["--clear", "Clear all tmp directory"],
	]

	arg_type = {
		"--mbed": "Use mbed-tools",
		"--latex": "Use texlive",
		"--python": "Use python",
		"--octave": "Use Octave",
		"--ngspice": "Use Ngspice",
	}

	arg_global_options = {
		"-s":
		{
			"help": "Save in current directory, by default is save in /tmp",
			"action": "store_true",
		},
	}

	arg_options = {
		"latex":
		{
			"-b":
			{
				"help": "Use biber",
				"action": "store_true",
			},
		},
		"mbed":
		{
			"-n":
			{
				"help": "Define the nucleo of STM",
				"action": "append",
				"metavar": "nucleo",
			},
		},
		"python":
		{
			"-p":
			{
				"help": "Run as project. Without run as script",
				"action": "store_true",
			},
		},
	}

	# Message of usage
	message_usage = "\n"
	message_usage = message_usage + "  %(prog)s [actions]\n"
	message_usage = message_usage + "  %(prog)s [type code] [options] [file]\n"

	# Create arguments
	parser = argparse.ArgumentParser(usage=message_usage )

	# Add File
	parser.add_argument("file", nargs='?', default='')

	# Add actions
	actions = parser.add_argument_group("actions")
	for act in arg_action :
		actions.add_argument(
			act[0],
			action="store_true",
			help = act[1],
		)

	# Add type
	type_code = parser.add_argument_group("type code")
	for opt, args in arg_type.items() :
		type_code.add_argument(opt, help=args, action="store_true")

	# Add global option
	global_options = parser.add_argument_group("global options")
	for opt, args in arg_global_options.items() :
		global_options.add_argument(opt, help=args["help"], action=args["action"])

	# Add options
	options = parser.add_argument_group("particular options")
	for file_type, args in arg_options.items() :
		type_options = parser.add_argument_group(file_type)
		for command, command_args in args.items() :
			type_options.add_argument(command, **command_args)

	return vars(parser.parse_args())
