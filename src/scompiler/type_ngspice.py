from pathlib import Path

import os
import subprocess

import scompiler


class TypeNgspice():
	def __init__(self):
		scompiler.message.process("Type: ngspice")

	def scompiler(self):
		run_script = scompiler.RunScript("ngspice", ".cir")
		run_script.run_script()
