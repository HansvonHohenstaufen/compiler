from pathlib import Path

import os
import subprocess

import scompiler


class RunScript():
	command = ""
	extension = ""

	# File
	file = ""
	file_name = ""
	# Directory
	tmp_dir = ""
	current_dir = ""
	current_dir_name = ""

	def __init__(self, command, extension):
		self.command = command
		self.extension = extension
		self.tmp_dir = "/tmp/scompiler/"+command

	def run_script(self):
		scompiler.message.process("Run as script")
		# Check file
		if self.__check_file():
			return -1
		# Check tmp directory
		if self.__make_tmp():
			return -2
		# Move to tmp directory
		command = "cp "+self.file+" "+self.tmp_dir+"/"
		if scompiler.utils.run_command(command):
			scompiler.message.error("Don't copy to tmp directory")
			return -3

		# Go to directory
		os.chdir(self.tmp_dir)

		# Run
		scompiler.message.step("Run")
		command = self.command + " "+self.file

		info = None
		command = command.split()
		if scompiler.configuration.getopt_save() :
			info = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		else :
			info = subprocess.run(command)

		if info.returncode :
			scompiler.message.error("Error when run python script")
			return -4
		scompiler.message.step("Run success!!!")

		# Save output
		if not scompiler.configuration.getopt_save() :
			return 0

		if scompiler.utils.save_files(self.file_name, self.current_dir, info.stdout.decode('utf8')) :
			return -5
		return 0


	def __check_file(self):
		scompiler.message.step("Check file")
		# Search file
		file = scompiler.configuration.get_file()
		if not file :
			scompiler.message.error("Don't define script file")
			return -1
		# Check file
		if not Path(file).is_file() :
			scompiler.message.error("Don't found file")
			return -2
		# Get name
		file = os.path.splitext(file)
		if file[1] != self.extension :
			scompiler.message.error("The file isn't "+extension+" file")
			return -3
		self.file = file[0] + file[1]
		self.file_name = file[0]
		return 0

	def __make_tmp(self):
		# Create tmp directory
		scompiler.message.step("Create tmp directory")
		self.current_dir = os.getcwd()
		self.current_dir_name = os.path.basename(self.current_dir)
		self.tmp_dir = self.tmp_dir+"/"+self.current_dir_name
		if not os.path.isdir(self.tmp_dir) :
			os.makedirs(self.tmp_dir)
		return 0
