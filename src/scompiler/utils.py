import subprocess
import os

import scompiler

def run_command(command, shell=False, show_error=True, show_output=False):
	if not shell :
		command = command.split()
	info = subprocess.run(command, shell=shell, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	# Check success
	if info.returncode :
		# Print errors
		if show_error :
			print(info.stderr.decode('utf8'))
		return -1
	if show_output and info.stdout.decode('utf8') :
		print(info.stdout.decode('utf8'))
	return 0

def clear_tmp():
	return run_command("rm -rf /tmp/scompiler")

def save_files(file_name, save_dir, output_text):
	scompiler.message.process("Save files")
	# Delete old directory
	scompiler.message.step("Delete all directory")
	if run_command("rm -rf log images") :
		scompiler.message.error("Error in delete old directory")
		return -1

	# Create directory
	scompiler.message.step("Create directory")
	os.makedirs("log")
	os.makedirs("images")
	if not os.path.isdir(save_dir+"/log") :
		os.makedirs(save_dir+"/log")
	if not os.path.isdir(save_dir+"/images") :
		os.makedirs(save_dir+"/images")

	# Move to directory
	scompiler.message.step("Move images")
	run_command("mv *.png *.jpg *.svg images/", shell=True, show_error=False)

	scompiler.message.step("Move logs")
	file_log = open("log/"+file_name+".log", "w")
	file_log.write(output_text)
	file_log.close()

	# Save
	scompiler.message.step("Save all")
	run_command("mv log/* "+save_dir+"/log/", shell=True, show_error=False)
	run_command("mv images/* "+save_dir+"/images/", shell=True, show_error=False)
	return 0
