import os


class Configuration:
	# Actions
	action_name = None
	# Type
	type_file = ""
	file = None
	# Options
	opt_biber = False
	opt_save = False
	opt_pkg = False

	opt_nucleo = ""

	def set_options(self, args):
		# Actions
		if args["version"] :
			self.action_name = "version"
			return
		if args["clear"] :
			self.action_name = "clear"
			return
		# Type
		self.type_file = self.__get_file(args)
		self.file = args["file"]

		# Option
		self.__get_options(args)

	def get_type(self):
		return self.type_file
	def get_action(self):
		return self.action_name
	def get_file(self):
		return self.file

	def getopt_biber(self):
		return self.opt_biber
	def getopt_pkg(self):
		return self.opt_pkg
	def getopt_save(self):
		return self.opt_save
	def getopt_nucleo(self):
		return self.opt_nucleo

	def __get_file(self, args):
		if args["mbed"] :
			return "mbed"
		if args["latex"] :
			return "latex"
		if args["python"] :
			return "python"
		if args["octave"] :
			return "octave"
		if args["ngspice"] :
			return "ngspice"
		return None

	def __get_options(self, args):
		if args["b"] :
			self.opt_biber = True
		if args["n"] :
			self.opt_nucleo = str(args["n"][0])
		if args["p"] :
			self.opt_pkg = True
		if args["s"] :
			self.opt_save = True
