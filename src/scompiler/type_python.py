from pathlib import Path

import os
import subprocess

import scompiler


class TypePython():
	tmp_dir = "/tmp/scompiler/python"
	pkg_dir = "/tmp/scompiler-test-py/"
	file = ""
	file_name = ""
	current_dir = ""
	current_dir_name = ""

	def __init__(self):
		scompiler.message.process("Type: Python")

	def scompiler(self):
		if scompiler.configuration.getopt_pkg() :
			return self.__run_pkg()
		else :
			return self.__run_script()

	def __run_pkg(self):
		scompiler.message.process("Run as pkg")
		# Check tmp directory
		if self.__make_tmp_pkg():
			return -1
		if self.__make_tmp():
			return -2
		# Move to tmp directory
		command = "cp -r "+self.current_dir+"/* "+self.tmp_dir+"/"
		if scompiler.utils.run_command(command, shell=True):
			scompiler.message.error("Don't copy to tmp directory")
			return -3

		# Go to pkg directory
		os.chdir(self.pkg_dir)
		# Make venv
		scompiler.message.step("Create venv")
		command = "python -m venv .venv"
		if scompiler.utils.run_command(command):
			scompiler.message.error("Don't copy to tmp directory")
			return -4

		# Install base package
		scompiler.message.step("Install packages")
		command = "source .venv/bin/activate;pip install build;"
		command = command + "cd "+self.tmp_dir+"; python3 -m build; pip install --force-reinstall dist/*.whl"
		if scompiler.utils.run_command(command, shell=True):
			scompiler.message.error("Don't copy to tmp directory")
			return -5

		scompiler.message.step("Install success!!!")

	def __run_script(self):
		run_script = scompiler.RunScript("python", ".py")
		run_script.run_script()

	def __check_file(self):
		# Search file
		file = scompiler.configuration.get_file()
		if not file :
			scompiler.message.error("Don't define script file")
			return -1
		# Check file
		if not Path(file).is_file() :
			scompiler.message.error("Don't found the python file")
			return -2
		# Get name
		file = os.path.splitext(file)
		if file[1] != ".py" :
			scompiler.message.error("The file isn't py file")
			return -3
		self.file = file[0] + file[1]
		self.file_name = file[0]
		return 0

	def __make_tmp(self):
		# Create tmp directory
		scompiler.message.step("Create tmp directory")
		self.current_dir = os.getcwd()
		self.current_dir_name = os.path.basename(self.current_dir)
		self.tmp_dir = self.tmp_dir+"/"+self.current_dir_name
		if not os.path.isdir(self.tmp_dir) :
			os.makedirs(self.tmp_dir)
		return 0

	def __make_tmp_pkg(self):
		# Create tmp test directory
		scompiler.message.step("Create tmp test directory")
		self.current_dir = os.getcwd()
		self.current_dir_name = os.path.basename(self.current_dir)
		if not os.path.isdir(self.pkg_dir) :
			os.makedirs(self.pkg_dir)
		return 0
