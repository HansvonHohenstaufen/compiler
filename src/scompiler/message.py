import subprocess

import scompiler


class Message:
	color = {
		"null":		"\x1b[1;0m",
		"bold":		"\x1b[01m",
		"Red":		"\x1b[1;31m",
		"red":		"\x1b[0;31m",
		"green":	"\x1b[0;32m",
		"Green":	"\x1b[1;32m",
		"yellow":	"\x1b[33m",
		"cyan":		"\x1b[36m"
	}

	def process(self, s):
		print(self.color["cyan"]+"[P] "+s+self.color["null"])
	def step(self, s):
		print(self.color["green"]+"[*] "+s+self.color["null"])
	def message(self, s):
		print(s)
	def warning(self, s):
		print(self.color["red"]+"[W] "+s+self.color["null"])
	def error(self, s):
		print(self.color["Red"]+"[E] "+s+self.color["null"])
