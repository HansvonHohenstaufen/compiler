from pkg_resources import get_distribution

import scompiler


def scompiler_main():
	# Get args
	args = scompiler.set_options()
	scompiler.configuration.set_options(args)

	# Actions
	match scompiler.configuration.get_action():
		case "version":
			version = get_distribution("scompiler").version
			print("scompiler-v"+version)
			return 0
		case "clear":
			scompiler.message.process("Clear tmp directory")
			return scompiler.utils.clear_tmp()

	# Compile by type
	program_scompiler = None
	match scompiler.configuration.get_type():
		case "mbed":
			program_scompiler = scompiler.TypeMbed()
		case "latex":
			program_scompiler = scompiler.TypeLatex()
		case "python":
			program_scompiler = scompiler.TypePython()
		case "octave":
			program_scompiler = scompiler.TypeOctave()
		case "ngspice":
			program_scompiler = scompiler.TypeNgspice()
		case other:
			scompiler.message.error("Don't define the type program")
			return -1;

	if program_scompiler.scompiler() :
		return -2

	return 0
