from pathlib import Path

import os
import subprocess
import tempfile

import scompiler


class TypeMbed():

	def __init__(self):
		scompiler.message.process("Type: Mbed")

	def scompiler(self):
		nucleo = scompiler.configuration.getopt_nucleo()
		if not nucleo :
			scompiler.message.error("Don't specific the STM targe targett")
			return -1

		# Create tmp build directory
		scompiler.message.step("Check build directory")
		tmp_dir = "/tmp/scompiler/mbed"
		if not os.path.isdir(tmp_dir) :
			os.makedirs(tmp_dir)

		if not os.path.islink("cmake_build") :
			link = Path("cmake_build")
			target = Path(tmp_dir+"/")
			link.symlink_to(target)

		# Compiler
		scompiler.message.step("Compile")
		command = "mbed-tools compile -m "+str(nucleo)+" -t GCC_ARM"
		if scompiler.utils.run_command(command):
			scompiler.message.error("Don't compile")
			return -2

		# Copy the output
		dest = "/tmp/"
		if scompiler.configuration.getopt_save():
			scompiler.message.step("Save bin")
			dest = "./"
		else :
			scompiler.message.step("Move bin to /tmp")

		command = "cp cmake_build/"+nucleo+"/develop/GCC_ARM/*.bin /tmp/stm_"+nucleo+".bin"
		info = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		# Check success
		if info.returncode :
			# Print errors
			print(info.stderr.decode('utf8'))
			scompiler.message.error("Don't copy")
			return -3
		return 0
