from pathlib import Path

import os
import subprocess

import scompiler


class TypeOctave():
	def __init__(self):
		scompiler.message.process("Type: Octave")

	def scompiler(self):
		run_script = scompiler.RunScript("octave", ".m")
		run_script.run_script()
