from pathlib import Path

import os
import subprocess

import scompiler


class TypeLatex():
	# Files
	file = ""
	file_name = ""
	# Directories
	tmp_dir = "/tmp/scompiler/latex"
	current_dir = ""
	current_dir_name = ""

	# Command
	command = ""

	def __init__(self):
		scompiler.message.process("Type: Latex")

	def scompiler(self):
		# Check file
		if self.__check_file():
			return -1
		# Check tmp directory
		if self.__make_tmp():
			return -2

		# Move to tmp directory
		command = "cp -r "+self.current_dir+" "+self.tmp_dir
		if scompiler.utils.run_command(command):
			scompiler.message.error("Don't copy to tmp directory")
			return -3

		# Go to directory
		os.chdir(self.tmp_dir+"/"+self.current_dir_name)

		# Compiler
		scompiler.message.step("Compile")
		self.command = "pdflatex -interaction=nonstopmode "+self.file_name

		if scompiler.utils.run_command(self.command):
			scompiler.message.error("Don't compile")
			return -4

		# Biber
		if scompiler.configuration.getopt_biber() :
			if self.__scompiler_biber():
				return -5

		# Delete metadata
		scompiler.message.step("Delete pdf metadata")
		command = "exiftool -all= -overwrite_original "+self.file_name+".pdf"
		if scompiler.utils.run_command(command):
			scompiler.message.error("Don't delete metadata")
			return -6

		# Move pdf
		dest = "/tmp/"
		if scompiler.configuration.getopt_save():
			scompiler.message.step("Save pdf")
			dest = self.current_dir + "/"
		else :
			scompiler.message.step("Move pdf to /tmp")

		command = "mv "+self.file_name+".pdf "+dest
		if scompiler.utils.run_command(command):
			scompiler.message.error("Don't move pdf file")
			return -7

		return 0

	def __check_file(self):
		# Search file
		if self.__search_main_latex() :
			scompiler.message.error("Don't found any tex file")
			return -1
		# Check file
		if not Path(self.file).is_file() :
			scompiler.message.error("The tex file isn't a file")
			return -2
		return 0

	def __make_tmp(self):
		# Create tmp directory
		scompiler.message.step("Check tmp directory")
		if not os.path.isdir(self.tmp_dir) :
			os.makedirs(self.tmp_dir)

		# Copy to tmp directory
		scompiler.message.step("Check copy to tmp directory")
		self.current_dir = os.getcwd()
		self.current_dir_name = os.path.basename(self.current_dir)
		return 0

	def __scompiler_biber(self):
		scompiler.message.step("Run biber and recompile")
		command_biber = "biber "+self.file_name

		if scompiler.utils.run_command(command_biber):
			scompiler.message.error("Don't run biber")
			return -1
		if scompiler.utils.run_command(self.command):
			scompiler.message.error("Don't compile")
			return -2
		if scompiler.utils.run_command(self.command):
			scompiler.message.error("Don't compile")
			return -3

	def __search_main_latex(self):
		file = scompiler.configuration.get_file()
		if file :
			self.file = file
			return 0

		scompiler.message.step("Search main latex file")

		for element in list(Path().glob('*')) :
			if os.path.isfile(element) :
				file = os.path.splitext(element)
				if file[1] == ".tex" :
					self.file = file[0] + file[1]
					self.file_name = file[0]
					return 0

		return -1
