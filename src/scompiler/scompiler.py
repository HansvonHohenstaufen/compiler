#!/usr/bin/env python

import sys

from scompiler.main import scompiler_main


def main():
	retval = scompiler_main()
	sys.exit(retval)

if __name__ == "__main__":
	main()
